### Powershell + Outlook for Data Exfiltration ###

Refer to the blogpost at http://niiconsulting.com/checkmate/2016/03/exfiltration-using-powershell-outlook/ for more details about the script


* ps-outlook-exfiltration.ps1 : Actual code used to exfiltrate data using MS-Outlook
* macro-exfiltration.vba : VBA code (to be embedded in MS Office documents) which is used as a delivery mechanism for the Powershell code